let http = require('http');

let courses = [
	{
		"courseName" : "HTML",
		"courseCategory": "front-end"
	},
	{
		"courseName" : "NodeJS",
		"courseCategory": "back-end"
	}
];

let port = 4000;

let server = http.createServer(function(request, response) {
	if (request.url == '/' && request.method == 'GET') {
		response.writeHead(200, {'Content-Type': 'text/plain'})
		response.end("Welcome to Booking System")
	} else if (request.url == '/profile' && request.method == 'GET') {
		response.writeHead(200, {'Content-Type': 'text/plain'})
		response.end("Welcome to your profile!")
	} else if (request.url == '/courses' && request.method == 'GET') {
		response.writeHead(200, {'Content-Type': 'text/plain'})
		response.end("Here's our courses available")
	} else if (request.url == '/addCourse' && request.method == 'POST') {
		let request_course = " ";

		request.on('data', function(data){
			request_course += data;
		});

		request.on('end', function(){
			request_course = JSON.parse(request_course)

			let new_course = {
				"courseName" : request_course.courseName,
				"courseCategory": request_course.courseCategory
			};

			console.log(new_course);
			courses.push(new_course);
			console.log(courses);


			response.writeHead(200, {'Content-Type': 'text/plain'})
			response.write(JSON.stringify(new_course))
			response.end("Add a course to our resources")
		})

		
	} else if (request.url == '/updateCourse' && request.method == "PUT") {
		response.writeHead(200, {'Content-Type': 'text/plain'})
		response.end("Update a course to our resoruces")
	} else if (request.url == '/archiveCourses' && request.method == "DELETE") {
		response.writeHead(200, {'Content-Type': 'text/plain'})
		response.end("Archive courses to our resoruces")
	}
});


server.listen(port);

console.log(`Server now accessible at localhost: ${port}`)